package connect;

public class AuthBook{
    private int au_bo_id;

    @Override
    public String toString() {
        return "AuthBook{" +
                "au_bo_id=" + au_bo_id +
                ", book_id=" + book_id +
                ", author_id=" + author_id +
                '}';
    }

    private int book_id;
    private int author_id;

    public int getAu_bo_id() {
        return au_bo_id;
    }

    public void setAu_bo_id(int au_bo_id) {
        this.au_bo_id = au_bo_id;
    }

    public int getBook_id() {
        return book_id;
    }

    public void setBook_id(int book_id) {
        this.book_id = book_id;
    }

    public int getAuthor_id() {
        return author_id;
    }

    public void setAuthor_id(int author_id) {
        this.author_id = author_id;
    }
}
