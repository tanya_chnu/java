package connect;

import org.testng.annotations.Test;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertTrue;

public class MyTest {

    @Test
    public void testForSearchBook(){
        Books b = new Books();
        b.setBook_id(1);
        b.setBook_name("The Harry Potter series");
        b.setYear(1997);
        Author au = new Author();
        au.setAuthor_id(1);
        au.setSurname("Rowling");
        List<Author> a =new ArrayList<>();
        a.add(au);
        b.setAuthors(a);
        DBConnection db = new DBConnection();
        try {
            assertEquals(db.searchBook("The Harry Potter series"), b);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testForSearchAuthor(){
        Author auth = new Author();
        auth.setAuthor_id(1);
        auth.setName("Joanne");
        auth.setSurname("Rowling");
        auth.setContry("England");
        DBConnection db = new DBConnection();
        try {
            assertEquals(db.searchAuthor("Rowling"), auth);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testAddBook() throws SQLException {
        DBConnection db = new DBConnection();
        Books b = new Books();
        b.setBook_name("Some book");
        b.setYear(2018);
        assertTrue(db.addBook(b));
    }


    @Test
    public void testAddAuthBook() throws SQLException {
        DBConnection db = new DBConnection();
        AuthBook ab = new AuthBook();
        //ab.setAu_bo_id(15);
        ab.setAuthor_id(1);
        ab.setBook_id(8);
        assertTrue(db.addAuthBook(ab));
    }

   @Test
    public void testDelete() throws SQLException {
        DBConnection db = new DBConnection();
        assertFalse(db.delete(5));
    }
}
