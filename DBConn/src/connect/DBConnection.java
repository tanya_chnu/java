package connect;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

class DBConnection {
   private static Connection connect() {
        Connection conn = null;
        try
        {
            String userName = "root";
            String password = "1234";
            String url = "jdbc:mysql://localhost/library";
            Class.forName ("com.mysql.jdbc.Driver").newInstance();
            conn = DriverManager.getConnection (url, userName, password);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return conn;
    }

    private void disconnect(Connection conn) throws SQLException {
        if (conn != null)
        {
            conn.close();

        }
    }

    boolean addBook(Books b) throws SQLException {
        String insert = "INSERT INTO books (book_name,year) VALUES(?, ?);";
        Connection conn = connect();
        conn.setAutoCommit(false);
        try (PreparedStatement psAB = conn.prepareStatement(insert)) {
            psAB.setString(1, b.getBook_name());
            psAB.setInt(2, b.getYear());
            psAB.executeUpdate();
            conn.commit();
            return psAB.executeUpdate() > 0;
        } finally {
            this.disconnect(conn);
        }
    }

    boolean addAuthBook(AuthBook ab) throws SQLException {
        String insert = "INSERT INTO authbook (author_id, book_id) VALUES(?, ?);";
        Connection conn = connect();
        conn.setAutoCommit(false);
        try (PreparedStatement psAB = conn.prepareStatement(insert)) {
            psAB.setInt(1, ab.getAuthor_id());
            psAB.setInt(2, ab.getBook_id());
            psAB.executeUpdate();
            conn.commit();
            return psAB.executeUpdate()>0;

        }
        finally {
            this.disconnect(conn);
        }
    }

    List<Books> searchBook(String bookName) throws SQLException {
        String search = "SELECT books.book_id,books.book_name,books.year,author.author_id,author.surname FROM books NATURAL JOIN authbook NATURAL JOIN author WHERE book_name=?;";
        Connection conn = connect();
        List<Books> result = new ArrayList<>();

        try (PreparedStatement psAB = conn.prepareStatement(search)) {
            psAB.setString(1, bookName);
            ResultSet rs = psAB.executeQuery();
            List<Books> book = new ArrayList<Books>();
            List<Author> auth = new ArrayList<Author>();
            if (rs != null) {
                while (rs.next()) {
                    Books b = new Books();
                    b.setBook_id(Integer.parseInt(rs.getString("book_id")));
                    b.setBook_name(rs.getString("book_name"));
                    b.setYear(Integer.parseInt(rs.getString("year")));
                    Author au = new Author();
                    au.setSurname(rs.getString("surname"));
                    auth.add(au);
                    book.add(b);
                }
                int k = 0;
                List<Author> au1 = new ArrayList<>();
                for (int i = 1; i < book.size(); i++) {
                    if (book.get(i).getBook_id() == book.get(i - 1).getBook_id()) {
                        au1.add(auth.get(k));
                        k++;
                    } else {
                        Books b1 = new Books();
                        b1.setAuthors(au1);
                        b1.setYear(book.get(i - 1).getYear());
                        b1.setBook_name(book.get(i - 1).getBook_name());
                        b1.setBook_id(book.get(i - 1).getBook_id());
                        result.add(b1);
                        au1 = new ArrayList<>();
                    }
                }

            }
            return result;
        }
        finally {
            this.disconnect(conn);
        }
    }

    Author searchAuthor(String surname) throws SQLException {
        String search = "SELECT * FROM author WHERE surname=\"?\";";
        Connection conn = connect();
        try (PreparedStatement psAB = conn.prepareStatement(search)) {
            psAB.setString(1, surname);
            Author au = new Author();
            ResultSet rs = psAB.executeQuery();
            if (rs != null) {
                au.setAuthor_id(Integer.parseInt(rs.getString("author_id")));
                au.setName(rs.getString("name"));
                au.setSurname(rs.getString("surname"));
                au.setContry(rs.getString("contry"));
            }
            return au;
        }
        finally {
            this.disconnect(conn);
        }
    }

    boolean delete(int id) throws SQLException {
        String delete = "DELETE FROM books WHERE book_id = ?;";
        Connection conn = connect();
        conn.setAutoCommit(false);
        try (PreparedStatement psB = conn.prepareStatement(delete)) {
            if (psB == null)
                return false;
            psB.setInt(1, id);
            psB.executeUpdate();
            conn.commit();
            return psB.executeUpdate()>0;
        }
        finally {
            this.disconnect(conn);
        }

    }
}
