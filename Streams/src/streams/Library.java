package streams;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Library {
    public String getAddress() {
        return address;
    }

    void setAddress(String address) {
        this.address = address;
    }

    private String address;

    public List<Book> getBooks() {
        return books;
    }

    void setBooks(List<Book> books) {
        this.books = books;
    }

    private List<Book> books;

    Library(){
        books=new ArrayList<>();
        address="NoName";
    }

    List<Book> searchBook(String bookName){
        return books.stream().filter((b)-> b.getName().equals(bookName))
                .collect(Collectors.toList());
    }

    List<Book> searchAuthor(ArrayList<String> sAuthor){
        return books.stream().filter((b)-> b.getAuthors().equals(sAuthor))
                .collect(Collectors.toList());
    }

    String addAuthor(String bName, String aName){
        for(Book elem : books){
            if(bName.equals(elem.getName())){
                elem.setAuthor(aName);
            }
        }
        return books.toString();
    }
    String addBook(String name, int pages, List<String> author, int year, String publ){
        Book b=new Book();
        b.setName(name);
        for(String au : author)
            b.setAuthor(au);
        b.setPages(pages);
        b.setPublishing(publ);
        b.setPublicationYear(year);
        if(books.stream().noneMatch(b::equals)) books.add(b);
        return books.toString();
    }
}
