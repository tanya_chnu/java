package streams;

import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.testng.AssertJUnit.assertEquals;

public class MyTest {

    private Library create() {
    Book b = new Book();
    b.setName("The Harry Potter series");
    b.setAuthor("Joanne Rowling");
    b.setPages(10000);
    b.setPublicationYear(1997);
    b.setPublishing("Some publ");

    Library library = new Library();
    library.setAddress("My Library");
    List<Book> books = new ArrayList<>();
    books.add(b);

    b = new Book();
    b.setName("A Game of Thrones");
    b.setAuthor("George Marlin");
    b.setPages(1000);
    b.setPublicationYear(1996);
    b.setPublishing("Some publ");
    books.add(b);

    b = new Book();
    b.setName("The Redbreast");
    b.setAuthor("Jo Nesbo");
    b.setPages(800);
    b.setPublicationYear(2009);
    b.setPublishing("Some publ");
    books.add(b);

    b = new Book();
    b.setName("The House of God");
    b.setAuthor("Samuel Shem");
    b.setPages(500);
    b.setPublicationYear(1978);
    b.setPublishing("Some publ");
    books.add(b);
    library.setBooks(books);
    return library;

}
    @Test
    public void testForSearchAuthor(){
        Book b = new Book();
        b.setName("The House of God");
        b.setAuthor("Samuel Shem");
        b.setPages(500);
        b.setPublicationYear(1978);
        b.setPublishing("Some publ");
        List<Book> books=new ArrayList<>();
        books.add(b);
        Library library = create();
        ArrayList<String> lst =b.getAuthors();
        assertEquals(library.searchAuthor(lst),books);
    }

    @Test
    public void testForSearchBook(){
        Book b = new Book();
        b.setName("The House of God");
        b.setAuthor("Samuel Shem");
        b.setPages(500);
        b.setPublicationYear(1978);
        b.setPublishing("Some publ");
        List<Book> books=new ArrayList<>();
        books.add(b);
        Library library = create();
        assertEquals(library.searchBook("Applied Maths"),books);
    }

    @Test
    public void testForAddAuthor(){
        Library library = new Library();
        Book b = new Book();
        b.setName("The House of God");
        b.setAuthor("Samuel Shem");
        b.setPages(500);
        b.setPublicationYear(1978);
        b.setPublishing("Some publ");
        List<Book> books=new ArrayList<>();
        books.add(b);
        library.setBooks(books);
        Book b1 = new Book();
        b1.setName("The House of God");
        b1.setAuthor("Samuel Shem");
        b1.setAuthor("New auth");
        b1.setPages(500);
        b1.setPublicationYear(1978);
        b1.setPublishing("Some publ");
        assertEquals(library.addAuthor("The House of God","New auth"),"["+b1.toString()+"]");
    }

    @Test
    public void tesrForAddBook(){
        Library library=create();
        List<Book> books = new ArrayList<>();
        Book b = new Book();
        b.setName("The Harry Potter series");
        b.setAuthor("Joanne Rowling");
        b.setPages(10000);
        b.setPublicationYear(1997);
        b.setPublishing("Some publ");
        books.add(b);
        b = new Book();
        b.setName("A Game of Thrones");
        b.setAuthor("George Marlin");
        b.setPages(1000);
        b.setPublicationYear(1996);
        b.setPublishing("Some publ");
        books.add(b);
        b = new Book();
        b.setName("The Redbreast");
        b.setAuthor("Jo Nesbo");
        b.setPages(800);
        b.setPublicationYear(2009);
        b.setPublishing("Some publ");
        books.add(b);
        b = new Book();
        b.setName("The House of God");
        b.setAuthor("Samuel Shem");
        b.setPages(500);
        b.setPublicationYear(1978);
        b.setPublishing("Some publ");
        books.add(b);
        Book b1 = new Book();
        b1.setName("New book");
        b1.setAuthor("Auth");
        b1.setPages(200);
        b1.setPublicationYear(1977);
        b1.setPublishing("Some publ");
        books.add(b1);
        List<String> auth=new ArrayList<>();
        auth.add("Auth");
        assertEquals(library.addBook("New book",200,auth,1977,"Some publ"),books.toString());
    }

}
