package streams;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Book {
    private String name;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Book)) return false;
        Book book = (Book) o;
        return getPages() == book.getPages() &&
                publicationYear == book.publicationYear &&
                Objects.equals(getName(), book.getName()) &&
                Objects.equals(author, book.author) &&
                Objects.equals(getPublishing(), book.getPublishing());
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, getName(), author, getPages(), publicationYear, getPublishing());
    }

    void setName(String name) {
        this.name = name;
    }

    void setAuthor(String author) {
        this.author.add(author);
    }

    void setPages(int pages) {
        this.pages = pages;
    }

    void setPublicationYear(int publicationYear) {
        this.publicationYear = publicationYear;
    }

    void setPublishing(String publishing) {
        this.publishing = publishing;
    }

    private List<String> author = new ArrayList<>();
    private int pages;
    private int publicationYear;
    private String publishing;

    public void addAuthor(String author){
        int k=0;
        for (String anAuthor : this.author){
            if(author.equals(anAuthor)) k++;
        }
        if(k==0) this.author.add(author);
    }

    String getName() {
        return name;
    }

    private int getPages() {
        return pages;
    }

    private int getYear() {
        return publicationYear;
    }

    private String getPublishing() {
        return publishing;
    }

    ArrayList<String> getAuthors() {
        ArrayList<String> auth = new ArrayList<String>();
        for (String anAuthor : this.author) {
            auth.add(anAuthor);
        }
        return auth;
    }

    public Builder newBuilder() {
        return new Book().new Builder();
    }

    public class Builder {

        public Builder setName(String name) {
            Book.this.name = name;
            return this;
        }

        public Builder setPages(int pages) {
            Book.this.pages = pages;
            return this;
        }

        public Builder setYear(int year) {
            Book.this.publicationYear = year;
            return this;
        }

        public Builder setAuthor(String author) {
            Book.this.author.add(author);
            return this;
        }

        public Book build() {
            return Book.this;
        }

    }

    @Override
    public String toString() {

        return "Book "+this.name+" was written by "+this.author.toString()+
                " and was publicised in "+this.getYear()+"in "+this.getPublishing();
    }


}
