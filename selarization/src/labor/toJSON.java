package labor;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import javax.xml.bind.JAXBException;
import java.io.*;

public class toJSON implements Serializer {
    @Override
    public void serializeBook(Book book, String path) {
        book.isValid();
            JSONObject obj = new JSONObject();
            obj.put("BookName", book.getName());
            JSONArray authors = new JSONArray();
            authors.addAll(book.getAuthors());
            obj.put("Authors", authors);
            obj.put("Pages", book.getPages());
            obj.put("PublicationYear", book.getYear());
            obj.put("Publisher", book.getPublishing());
            JSONArray array = new JSONArray();
            array.add(obj);
            writeJsonFile(new File(path), array.toString());
    }

    @Override
    public Book deserializeBook(String path) throws IOException, JAXBException {
        File fl = new File(path);
        FileInputStream fin = new FileInputStream(fl);
        BufferedReader reader = new BufferedReader(new InputStreamReader(fin));
        Book book = new Book();
        book.setName(reader.readLine());
        book.setAuthor(reader.readLine());
        book.setPages(Integer.parseInt(reader.readLine()));
        book.setPublicationYear(Integer.parseInt(reader.readLine()));
        book.setPublishing(reader.readLine());
        return book;
    }

    public static void writeJsonFile(File file, String json) {
        BufferedWriter bufferedWriter = null;
        try {

            if (!file.exists()) {
                file.createNewFile();
            }

            FileWriter fileWriter = new FileWriter(file);
            bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write(json);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bufferedWriter != null) {
                    bufferedWriter.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
