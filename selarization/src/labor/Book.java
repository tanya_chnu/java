package labor;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Book {
    private String name;
    private List<String> author = new ArrayList<>();
    private int pages;
    private int publicationYear;
    private String publishing;

    public void setName(String name) {
        this.name = name;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public void setPublicationYear(int publicationYear) {
        this.publicationYear = publicationYear;
    }

    public void setPublishing(String publishing) {
        this.publishing = publishing;
    }

    public void setAuthor(String nAuthor) {
        this.author.add(nAuthor);
    }

    public void addAuthor(String author){
        int k=0;
        for (String anAuthor : this.author){
            if(author==anAuthor) k++;
        }
        if(k==0) this.author.add(author);
    }

    public String getName() {
        return name;
    }

    public int getPages() {
        return pages;
    }

    public int getYear() {
        return publicationYear;
    }

    public String getPublishing() {
        return publishing;
    }

    public ArrayList<String> getAuthors() {
        ArrayList<String> auth = new ArrayList<String>();
        for (String anAuthor : this.author) {
            auth.add(anAuthor);
        }
        return auth;
    }

    @Override
    public String toString() {
        return "Book "+this.name+" was written by "+this.author.toString()+" and was publicated in "+this.publicationYear;
    }

    public boolean validateAuthors() {
        String regx = "(^[А-Я]{1}[а-я]{1,14} [А-Я]{1}[а-я]{1,14}$)";
        for (String auth : this.getAuthors()) {
            Pattern pattern = Pattern.compile(regx, Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(auth);
            if (!matcher.find()) return matcher.find();
        }
        return true;
    }

    public boolean validatePages() {
        String regx = "(^[1-9]{1}[0-9]{0,5}$)";
        Pattern pattern = Pattern.compile(regx, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(String.valueOf(this.getPages()));
        return matcher.find();
    }

    public boolean validateYear() {
        String regx = "(^[1-2]{1}[0-9]{0,3})";
        Pattern pattern = Pattern.compile(regx, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(String.valueOf(this.getYear()));
        return matcher.find();
    }

    public boolean validatePublish(){
        String regx = "([A-Za-z0-9])";
        Pattern pattern = Pattern.compile(regx, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(this.getPublishing());
        return matcher.find();
    }

    public void isValid(){
        if(!this.validateAuthors()){
            throw new ArithmeticException("check list of authors");
        }
        if(!this.validatePages()){
            throw new ArithmeticException("check number of pages");
        }
        if(!this.validateYear()){
            throw new ArithmeticException("check year of publishing");
        }
        if(!this.validatePublish()){
            throw new ArithmeticException("check name of publish");
        }
    }
}
