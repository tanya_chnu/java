package labor;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.IOException;

public class toXML implements Serializer {
        @Override
        public void serializeBook(Book book, String path)  {
            book.isValid();
            try {
                JAXBContext jaxbContext = JAXBContext.newInstance(Book.class);
                Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
                jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
                jaxbMarshaller.marshal(book, new File(path));

            }
            catch (JAXBException e) {
                e.printStackTrace();
            }}

        @Override
        public Book deserializeBook(String path) throws IOException, JAXBException {
            JAXBContext jaxbContext = JAXBContext.newInstance(Book.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            Book book = (Book) jaxbUnmarshaller.unmarshal(new File(path));
            return book;
        }
}
