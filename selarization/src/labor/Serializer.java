package labor;

import javax.xml.bind.JAXBException;
import java.io.IOException;

public interface Serializer {
    Book deserializeBook(String path) throws IOException, JAXBException;

    void serializeBook(Book objsToSerial, String path) throws IOException, JAXBException;
}
