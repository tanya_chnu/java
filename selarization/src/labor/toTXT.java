package labor;


import javax.xml.bind.JAXBException;
import java.io.*;

public class toTXT implements Serializer {
    @Override
    public void serializeBook(Book book, String path)  {
        book.isValid();
        try{

            FileWriter fileWriter = new FileWriter(path, true);

            fileWriter.write(book.getName());
            fileWriter.write(book.getPages());
            fileWriter.write(book.getYear());
            String s= new String();
            for(String au:book.getAuthors()){
                s+=au+' ';
            }
            fileWriter.write(s);
            fileWriter.write(book.getPublishing());
            fileWriter.close();

        }
       catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Book deserializeBook(String path) throws IOException, JAXBException {
        BufferedReader br = new BufferedReader(new FileReader(new File((path))));
        Book book = new Book();
        book.setName(br.readLine());
        book.setAuthor(br.readLine());
        book.setPages(Integer.parseInt(br.readLine()));
        book.setPublicationYear(Integer.parseInt(br.readLine()));
        book.setPublishing(br.readLine());
        return book;
    }
}
