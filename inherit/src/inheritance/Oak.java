package inheritance;

public class Oak extends Tree {
    private String name;
    private int years_lived;

    private String getName() {
        return name;
    }

    private void setName(String name) {
        this.name = name;
    }

    private int getYears_lived() {
        return years_lived;
    }

    private void setYears_lived(int years_lived) {
        this.years_lived = years_lived;
    }

    public void set_info(String name, int years, double h, double w, String color, int count, double square, String place){
        this.setName(name);
        this.setYears_lived(years);
        this.setHeight(h);
        this.setWidth(w);
        this.setColor_leaf(color);
        this.setCount(count);
        this.set_inform(square, place);
    }

    public String toString(){
        return "Name "+this.getName()+", years "+this.getYears_lived()+
                ", height "+this.getHeight()+", width "+this.getWidth()+
                ", leaf color "+this.getColor_leaf();
    }
}
