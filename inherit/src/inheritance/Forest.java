package inheritance;

public class Forest {
    private double square;

    double getSquare() {
        return square;
    }

    private void setSquare(double square) {
        this.square = square;
    }

    String getPlace() {
        return place;
    }

    private void setPlace(String place) {
        this.place = place;
    }

    private String place;

    public void set_inform(double square, String place){
        this.setPlace(place);
        this.setSquare(square);
    }
}
