package inheritance;

class Tree extends Forest {
    private double height;
    private int count;

    private int getCount() {
        return count;
    }

    void setCount(int count) {
        this.count = count;
    }

    double getHeight() {
        return height;
    }

    void setHeight(double height) {
        this.height = height;
    }

    double getWidth() {
        return width;
    }

    void setWidth(double width) {
        this.width = width;
    }

    String getColor_leaf() {
        return color_leaf;
    }

    void setColor_leaf(String color_leaf) {
        this.color_leaf = color_leaf;
    }

    private double width;
    private String color_leaf;

    public String toString(){
        return "Forest square "+this.getSquare()+", is in "+this.getPlace()+
                ", with "+this.getCount();
    }

}
