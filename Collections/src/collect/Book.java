package collect;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Book {
    private String name;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Book)) return false;
        Book book = (Book) o;
        return getPages() == book.getPages() &&
                publicationYear == book.publicationYear &&
                Objects.equals(getName(), book.getName()) &&
                Objects.equals(author, book.author) &&
                Objects.equals(getPublishing(), book.getPublishing());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getName(), author, getPages(), publicationYear, getPublishing());
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAuthor(String author) {
        this.author.add(author);
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public void setPublicationYear(int publicationYear) {
        this.publicationYear = publicationYear;
    }

    public void setPublishing(String publishing) {
        this.publishing = publishing;
    }

    private List<String> author = new ArrayList<>();
    private int pages;
    private int publicationYear;
    private String publishing;

    public void addAuthor(String author){
        int k=0;
        for (String anAuthor : this.author){
            if(author==anAuthor) k++;
        }
        if(k==0) this.author.add(author);
    }

    public String getName() {
        return name;
    }

    public int getPages() {
        return pages;
    }

    public int getYear() {
        return publicationYear;
    }

    public String getPublishing() {
        return publishing;
    }

    public ArrayList<String> getAuthors() {
        ArrayList<String> auth = new ArrayList<String>();
        for (String anAuthor : this.author) {
            auth.add(anAuthor);
        }
        return auth;
    }

    public Builder newBuilder() {
        return new Book().new Builder();
    }

    public class Builder {

        public Builder setName(String name) {
            Book.this.name = name;
            return this;
        }

        public Builder setPages(int pages) {
            Book.this.pages = pages;
            return this;
        }

        public Builder setYear(int year) {
            Book.this.publicationYear = year;
            return this;
        }

        public Builder setAuthor(String author) {
            Book.this.author.add(author);
            return this;
        }

        public Book build() {
            return Book.this;
        }

    }

    @Override
    public String toString() {

        return "Book "+this.name+" was written by "+this.author.toString()+" and was publicated in "+this.publicationYear;
    }


}
