package collect;

import java.util.ArrayList;
import java.util.List;

public class Library {
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    private String address;

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    List<Book> books;

    public Library(){
        books=new ArrayList<>();
        address="NoName";
    }

    public String searchBook(String bookName){
        for(Book elem : books){
            if(bookName.equals(elem.getName())) {
                return elem.toString();
            }
        }
        return "This book does not exist in this library";
    }

    public String searchAuthor(String sAuthor){
        for(Book elem : books) {
            for (String auth : elem.getAuthors()) {
                if (sAuthor.equals(auth)) {
                    return elem.toString();
                }
            }
        }
        return "There is not such author";
    }

    public String addAuthor(String bName, String aName){
        for(Book elem : books){
            if(bName.equals(elem.getName())){
                elem.setAuthor(aName);
            }
        }
        return books.toString();
    }
    public String addBook(String name, int pages, List<String> author, int year, String publ){
        Book b=new Book();
        for(Book elem : books){
            if(name.equals(elem.getName())) return books.toString();
        }
        b.setName(name);
        for(String au : author)
            b.setAuthor(au);
        b.setPages(pages);
        b.setPublishing(publ);
        b.setPublicationYear(year);
        books.add(b);
        return books.toString();
    }
}
